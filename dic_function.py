

def dict_contains_key(d_arg, k_arg):
    "dict_contains_key:"
    "checks if the given dictionary contains the given key"
    "d_arg: dictionary to search the key inside"
    "k_arg: the searched key"
    "returns true if contains otherwise returns false"
    if k_arg in d_arg.keys():
        return True
    else:
        return False

d = { 1 : 'one', 2 : { 'a' : 'aba' } , 3 : [1,2,3] }

k = int(input("Enter id number:"))
if dict_contains_key(d, k):
    print(f'for key {k} the value is {d[k]}')
else:
    print(f'key {k} was not found')

if k in d.keys():
    print(f'for key {k} the value is {d[k]}')
else:
    print(f'key {k} was not found')