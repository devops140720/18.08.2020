
# Global Scope
x = 1 # here everything is in global scope
y = 2 # here everything is in global scope

def foo(x):
    print(f'x inside function foo is {x}. this is a local x')
    x = 5
    print(f'x inside function foo is {x}. this is a local x')

def foo_adv_read_only():
    # 1. access field value before putting new value
    #    field is read - only! be carefull!
    print(f'x inside foo_avd {x}') # local x not found, global x will be used
    x = 10 # craash!!v
    print(f'x inside foo_avd {x}')

def foo_adv_new_x():
    # 2. access field first time with new value
    #    field is local!!!
    x = 10 # this will make a local x
    print(f'x inside foo_avd {x}')

def foo_global_x():
    # 3. the function x will be same as global
    global x
    x = 77

#foo(3)
#foo(x)
#foo_adv_read_only()
foo_adv_new_x()
foo_global_x()
print('outside the function - global scope, the x is', x)
print('outside the function - global scope, the y is', y)
